compile (needs libpcap installed):

`gcc -o clk_sync_observer -pedantic -lpcap ./clk_sync_observer.c`

run:

`./clk_sync_observer`
